const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class ProductDetailPage extends Page {
    /**
     * selectors using getter methods
     */

    get productTitle() {
        return $("h1.product-title")
    }

    get productPrice () {
        return $(".product-primary-info span:nth-child(1)");
    }

    getProductPageTitle(){
        this.productTitle.waitForDisplayed();
        return this.productTitle.getText();
    }

    getProductPagePrice(){
        this.productPrice.waitForDisplayed();
        return this.productPrice.getText();
    }

         /**
     * URL product detail
     * @param {String} urlProductDetail the text to search
     */
    open (urlproductDetail) {
        return super.open(urlproductDetail);
    }
}

module.exports = new ProductDetailPage();
