const Page = require('./page');

//Selector to the first result of the search
const searchFirstResultSelector = ".search-results-products-container > div:nth-child(1)";
/**
 * sub page containing specific selectors and methods for a specific page
 */
class HomePage extends Page {
    /**
     * selectors using getter methods
     */

    get searchBarInput () {
        return $(".search-bar input");
    }

    get searchButton(){
        return $(".search-bar  svg > path");
    }
    
    get searchFirstResultProductTitle() {
        return $(searchFirstResultSelector +" h2")
    }

    get searchFirstResultPrice () {
        return $(searchFirstResultSelector +" .price span:nth-child(1)");
    }

    get searchFirstResultUrl(){
        return $(searchFirstResultSelector+" #title-pdp-link");
    }

     // methods to encapsule automation code to interact with the page


    firstResultPriceExiststs(){
        this.searchFirstResultPrice.waitForExist();
        return this.searchFirstResultPrice.isExisting();
    }

    clickOnFirstResult(){
        this.searchFirstResultProductTitle.waitForDisplayed();
        this.searchFirstResultProductTitle.click();
    }

    clickOnSearchBar () {
        this.searchBarInput.waitForDisplayed();
        this.searchBarInput.click();
    }

     /**
     * Enter text in the search bar
     * @param {String} text the text to search
     */
    search(text){
        this.clickOnSearchBar();
        this.searchBarInput.setValue(text);
        browser.keys("Enter"); //Another way to search.
        //this.searchButton.click();
    }

    getSearchFirstResultPrice(){
        return this.searchFirstResultPrice.getText();
    }

    getSearchFirstResultTitle(){
        return this.searchFirstResultProductTitle.getText();
    }

    getSearchFirstResultUrl(){
        return this.searchFirstResultUrl.getAttribute("href");
    }

    open () {
        return super.open(`${browser.options.baseUrl}`);
    }
}

module.exports = new HomePage();
