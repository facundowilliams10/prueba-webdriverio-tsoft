const { expect } = require('chai');

const homePage = require('../../pageobjects/home.page');
const productDetailPage = require('../../pageobjects/productDetail.page');

describe('Prueba WebdriverIO Facundo Williams - TSFOT', function (){
    let firstResultTitle = "var inicialized";
    let firstResultPrice = "var inicialized";
    let firstResultUrl = "var inicialized";

    it("atc01: Price in the first product should exist", () => {
        homePage.open();
        browser.pause(10000);//wait for adds to disappear (Explicit Wait)
        homePage.search("pintura blanca");
        
        //These values are saved for future validations
        firstResultTitle = homePage.getSearchFirstResultTitle();
        firstResultUrl = homePage.getSearchFirstResultUrl();
        firstResultPrice = homePage.getSearchFirstResultPrice();

        //Validation
        expect(homePage.firstResultPriceExiststs());
    });

    it("atc02: Title in the first product result should be equal to title in product detail", () => {
        productDetailPage.open(firstResultUrl);
        let productDetailTitle = productDetailPage.getProductPageTitle();
        //Validation
        expect(productDetailTitle).to.equal(firstResultTitle);
    });

    
    it("atc03: Price in the first product result should be equal to price in product detail", () => {
        productDetailPage.open(firstResultUrl);
        let productDetailPrice = productDetailPage.getProductPagePrice();
        //Validation
        expect(productDetailPrice).to.equal(firstResultPrice);
    });

});
